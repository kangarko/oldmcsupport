package org.bukkit.scheduler;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.scheduler.CraftScheduler;
import org.bukkit.plugin.Plugin;

/**
 * This class is provided as an easy way to handle scheduling tasks.
 *
 * Compatibility layer: Add +5 ticks and then return the pending task
 */
public abstract class BukkitRunnable implements Runnable {

	private int taskId = -1;

	/**
	 * Attempts to cancel this task.
	 *
	 * @throws IllegalStateException if task was not scheduled yet
	 */
	public synchronized void cancel() throws IllegalStateException {
		Bukkit.getScheduler().cancelTask(this.getTaskId());
	}

	/**
	 * Schedules this in the Bukkit scheduler to run on next tick.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTask(Plugin plugin) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleSyncRepeatingTask(plugin, this, 0, -1));
	}

	/**
	 * <b>Asynchronous tasks should never access any API in Bukkit. Great care
	 * should be taken to assure the thread-safety of asynchronous tasks.</b>
	 * <p>
	 * Schedules this in the Bukkit scheduler to run asynchronously.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTaskAsynchronously(Plugin plugin) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleAsyncRepeatingTask(plugin, this, 0, -1));
	}

	/**
	 * Schedules this to run after the specified number of server ticks.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @param delay the ticks to wait before running the task
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTaskLater(Plugin plugin, long delay) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleSyncRepeatingTask(plugin, this, delay, -1));
	}

	/**
	 * <b>Asynchronous tasks should never access any API in Bukkit. Great care
	 * should be taken to assure the thread-safety of asynchronous tasks.</b>
	 * <p>
	 * Schedules this to run asynchronously after the specified number of
	 * server ticks.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @param delay the ticks to wait before running the task
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTaskLaterAsynchronously(Plugin plugin, long delay) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleAsyncRepeatingTask(plugin, this, delay, -1));
	}

	/**
	 * Schedules this to repeatedly run until cancelled, starting after the
	 * specified number of server ticks.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @param delay the ticks to wait before running the task
	 * @param period the ticks to wait between runs
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTaskTimer(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleSyncRepeatingTask(plugin, this, delay + 5, period));
	}

	/**
	 * <b>Asynchronous tasks should never access any API in Bukkit. Great care
	 * should be taken to assure the thread-safety of asynchronous tasks.</b>
	 * <p>
	 * Schedules this to repeatedly run asynchronously until cancelled,
	 * starting after the specified number of server ticks.
	 *
	 * @param plugin the reference to the plugin scheduling task
	 * @param delay the ticks to wait before running the task for the first
	 *     time
	 * @param period the ticks to wait between runs
	 * @return a BukkitTask that contains the id number
	 * @throws IllegalArgumentException if plugin is null
	 * @throws IllegalStateException if this was already scheduled
	 */
	public synchronized BukkitTask runTaskTimerAsynchronously(Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
		this.checkState();
		return this.setupId(((CraftScheduler) Bukkit.getScheduler()).newScheduleAsyncRepeatingTask(plugin, this, delay + 2, period));
	}

	/**
	 * Gets the task id for this runnable.
	 *
	 * @return the task id that this runnable was scheduled as
	 * @throws IllegalStateException if task was not scheduled yet
	 */
	public synchronized int getTaskId() throws IllegalStateException {
		final int id = this.taskId;
		if (id == -1)
			throw new IllegalStateException("Not scheduled yet");
		return id;
	}

	private void checkState() {
		if (this.taskId != -1)
			throw new IllegalStateException("Already scheduled as " + this.taskId);
	}

	private BukkitTask setupId(final BukkitTask task) {
		this.taskId = task.getTaskId();

		return task;
	}
}