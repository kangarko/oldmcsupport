package org.mineacademy.oldmcsupport;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class OldMCSupport extends JavaPlugin implements Listener {

	@Override
	public void onLoad() {
		if (!this.getServer().getVersion().contains("1.2.5"))
			throw new RuntimeException("This plugin only works with Minecraft 1.2.5");
	}

	@Override
	public void onEnable() {
		this.getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler(ignoreCancelled = false, priority = EventPriority.LOWEST)
	public void onChat(PlayerChatEvent oldEvent) {
		final AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(oldEvent.getPlayer(), oldEvent.getMessage(), oldEvent.getRecipients());
		event.setFormat(oldEvent.getFormat());

		Bukkit.getPluginManager().callEvent(event);

		oldEvent.setCancelled(event.isCancelled());
		oldEvent.setFormat(event.getFormat());
		oldEvent.setMessage(event.getMessage());
		oldEvent.setPlayer(event.getPlayer());
	}
}
